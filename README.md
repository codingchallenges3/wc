# Word count challenge

See https://codingchallenges.fyi/challenges/challenge-wc for the details.

## Running

To run the program on the sample file, use the following command to produce all counts:

`./gradlew run -q --args="/Users/martinvanvliet/Dev/Code/CodingChallenges/wc/test.txt"`

When things work properly, the following command shows the same output:

`wc test.txt`

## TODO

* Check the code into your personal github
