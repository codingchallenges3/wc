package wc;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Spec;
import picocli.CommandLine.Model.CommandSpec;

import java.io.File;
import java.nio.file.Files;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Command(name = "wc", mixinStandardHelpOptions = true, version = "wc 1.0",
         description = "Prints the wordcount of a file to STDOUT.")
public class App implements Callable<Integer> {
    @Spec CommandSpec spec;
    
    @Parameters(index = "0", description = "The file whose words to count.", arity="0..1")
    protected File file;

    @Option(names = "-c", description = "Count bytes")
    protected boolean countBytes = false;

    @Option(names = "-l", description = "Count lines")
    protected boolean countLines = false;

    @Option(names = "-w", description = "Count words")
    protected boolean countWords = false;

    @Option(names = "-m", description = "Count characters")
    protected boolean countCharacters = false;

    public static void main(String... args) {
        int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception {
        if (! (countBytes || countLines || countWords)) {
            countBytes = countLines = countWords = true;
        }

        long byteCount, lineCount, wordCount;
        byteCount = lineCount = wordCount = 0;

        byte[] inputAsBytes;
        String inputName = null;

        // No file, read from stdin
        if (file == null) {
            inputName = "stdin";
            inputAsBytes = System.in.readAllBytes();
        } else {
            inputName = file.getName();
            inputAsBytes = Files.readAllBytes(file.toPath());
        }

        String[] inputAsStrings = new String(inputAsBytes).split("\n");

        if (countBytes) {
            // If you need bytes, will need to read file twice
            byteCount = inputAsBytes.length;
        }
        if (countLines) {
            lineCount = inputAsStrings.length;
        }
        if (countWords) {
            wordCount = 0;
            Pattern p = Pattern.compile("[^\\s]+");
            for (String l : inputAsStrings) {
                Matcher m = p.matcher(l);
                wordCount += m.results().count();
            }
        }
        if (countCharacters) {
            int chars = 0;
            for (String l : inputAsStrings) {
                chars += l.toCharArray().length + 1; // the extra +1 is for the <enter> (CR/LF) character
            }
            // Print here and exit
            spec.commandLine().getOut().printf("     %d %s\n", chars, inputName);
            return 0;
        }

        // Print
        if (countBytes && countLines && countWords) {
            spec.commandLine().getOut().printf("     %d %d %d %s\n", lineCount, wordCount, byteCount, inputName);
        } else {
            long count = (countBytes ? byteCount : (countLines ? lineCount : wordCount));
            spec.commandLine().getOut().printf("     %d %s\n", count, inputName);
        }
        return 0;
    }
}
